PADDLE_COLOR  EQU 55
PADDLE_HEIGHT EQU 25
PADDLE_SPEED  EQU 3
PADDLE_OFFSET EQU 20
BALL_COLOR EQU 50
SCORE_COLOR EQU 59
SCORE_SUPPRESSION_MARGIN EQU 10

pong_tick:
	call _pong_input
	call _pong_update
	call _pong_draw
	call audio_play_tick
	
	ret
; end

_pong_input:
	mov al, [KEY_STATE+KEY_UP]
	cmp al, 0
	jne .move_p2_up
	
	mov al, [KEY_STATE+KEY_DOWN]
	cmp al, 0
	jne .move_p2_down
	
	mov [PLAYER_2.VY], word 0
	jmp .check_p1
	
	.move_p2_up:
		mov [PLAYER_2.VY], word -PADDLE_SPEED
		jmp .check_p1
	
	.move_p2_down:
		mov [PLAYER_2.VY], word PADDLE_SPEED
		jmp .check_p1
	
	.check_p1:
	
	mov al, [KEY_STATE+KEY_W]
	cmp al, 0
	jne .move_p1_up
	
	mov al, [KEY_STATE+KEY_S]
	cmp al, 0
	jne .move_p1_down
	
	mov [PLAYER_1.VY], word 0
	jmp .ret
	
	.move_p1_up:
		mov [PLAYER_1.VY], word -PADDLE_SPEED
		jmp .ret
	
	.move_p1_down:
		mov [PLAYER_1.VY], word PADDLE_SPEED
		jmp .ret
	
	.ret:
	ret
; end

_pong_update:
	push di
	
	mov ax, [PLAYER_1.Y]
	mov cx, [PLAYER_1.VY]
	add ax, cx
	mov cx, 0
	mov dx, SCREEN_HEIGHT - PADDLE_HEIGHT + 1
	call utils_clip_to_bounds
	mov [PLAYER_1.Y], ax
	
	mov ax, [PLAYER_2.Y]
	mov cx, [PLAYER_2.VY]
	add ax, cx
	mov cx, 0
	mov dx, SCREEN_HEIGHT - PADDLE_HEIGHT + 1
	call utils_clip_to_bounds
	mov [PLAYER_2.Y], ax
	
	mov ax, [BALL.X]
	mov cx, [BALL.VX]
	add ax, cx
	
	cmp ax, 0
	jle .player_2_scored
	
	cmp ax, SCREEN_WIDTH-1
	jge .player_1_scored
	
	; Unsuppress scoring if ball is between players
	
	mov cx, [PLAYER_1.X]
	add cx, SCORE_SUPPRESSION_MARGIN
	cmp ax, cx
	jle .clip_ball_x
	
	mov cx, [PLAYER_2.X]
	sub cx, SCORE_SUPPRESSION_MARGIN
	cmp ax, cx
	jge .clip_ball_x
	
	mov [SCORE_SUPPRESS], word 0
	
	jmp .clip_ball_x
	
	.player_1_scored:
		mov di, PLAYER_1
		jmp .post_scored
	
	.player_2_scored:
		mov di, PLAYER_2
		jmp .post_scored
	
	.post_scored:
		mov cx, [SCORE_SUPPRESS]
		cmp cx, 0
		jne .score_suppressed
		
		mov [SCORE_SUPPRESS], word 1
		
		; add point
		mov cx, [di+8]
		add cx, 1
		mov [di+8], cx
		
		; play tune
		push ax
		mov ax, SOUND_BOUNCE_POINT
		call audio_play_sound
		pop ax
		
		jmp .clip_ball_x
	
	.score_suppressed:
		push ax
		mov ax, SOUND_BOUNCE_WALL
		call audio_play_sound
		pop ax
		jmp .clip_ball_x
	
	.clip_ball_x:
		mov cx, 0
		mov dx, SCREEN_WIDTH
		call utils_clip_to_bounds
		mov [BALL.X], ax
		
		cmp cx, 0
		jz .check_ball_y
		
		mov cx, [BALL.VX]
		neg cx
		mov [BALL.VX], cx
	
	.check_ball_y:
		mov ax, [BALL.Y]
		mov cx, [BALL.VY]
		add ax, cx
		mov cx, 0
		mov dx, SCREEN_HEIGHT
		call utils_clip_to_bounds
		mov [BALL.Y], ax
	
	cmp cx, 0
	jz .paddle_collisions
	
	mov ax, SOUND_BOUNCE_WALL
	call audio_play_sound
	
	mov cx, [BALL.VY]
	neg cx
	mov [BALL.VY], cx
	
	.paddle_collisions:
		mov ax, PLAYER_1
		call _pong_handle_paddle_collision
		
		mov ax, PLAYER_2
		call _pong_handle_paddle_collision
	
	pop di
	
	ret
; end

_pong_handle_paddle_collision:
	; void (player* paddle[ax])
	
	; TODO: Use fixed point
	
	push bp
	mov bp, sp
	
	push bx
	push di
	
	; if ball.vx = 0:
	; 	jmp .ret
	mov cx, [BALL.VX]
	cmp cx, 0
	jz .ret
	
	; di = paddle
	mov di, ax
	
	; t[ax] = (paddle.x - ball.x) / ball.vx
	mov ax, [di+0]
	mov cx, [BALL.X]
	sub ax, cx
	mov dx, 0
	mov cx, [BALL.VX]
	idiv cx
	
	; if t not in [0, 1]:
	; 	jmp .ret
	cmp ax, 0
	jl .ret
	cmp ax, 1
	jg .ret
	
	; y[cx] = ball.y + t ball.vy
	mov cx, [BALL.VY]
	imul cx, ax
	mov dx, [BALL.Y]
	add cx, dx
	
	; 0 < paddle.y - y
	
	; if y < paddle.y:
	; 	jmp .ret
	; (dx = paddle.y - y)
	mov dx, [di+2]
	sub dx, cx
	jg .ret
	
	; if y > paddle.y + PADDLE_HEIGHT:
	; 	jmp .ret
	add dx, PADDLE_HEIGHT
	jl .ret
	
	.collision:
		; ball.vx := t ball.vx - (1 - t) ball.vx
		; ball.vy := t ball.vy + (1 - t) (ball.vy + paddle.vy)
		
		; ball.vx *= (2 t - 1)
		mov cx, ax
		mov dx, 2
		imul cx, dx
		sub cx, 1
		mov dx, [BALL.VX]
		imul dx, cx
		mov [BALL.VX], dx
		
		; ball.vy += (1 - t) paddle.vy
		mov dx, [di+6]
		mov cx, 1
		sub cx, ax
		imul dx, cx
		mov cx, [BALL.VY]
		add dx, cx
		mov [BALL.VY], dx
		
		mov ax, SOUND_BOUNCE_PADDLE
		call audio_play_sound
		
		jmp .ret
	
	.ret:
	
	pop di
	pop bx
	
	mov sp, bp
	pop bp
	
	ret
;

_pong_draw:
	call video_clear_screen
	
	call _pong_draw_scores
	
	push PLAYER_1
	call _pong_draw_paddle
	add sp, 2
	
	push PLAYER_2
	call _pong_draw_paddle
	add sp, 2
	
	call _pong_draw_ball
	
	call video_copy_buffer_to_screen
	
	ret
; end

_pong_draw_scores:
	mov ax, [PLAYER_1.SCORE]
	mov cx, SCREEN_WIDTH/2 - 10
	mov dx, 10
	call numbers_draw_digit
	
	mov ax, [PLAYER_2.SCORE]
	mov cx, SCREEN_WIDTH/2 + 3
	mov dx, 10
	call numbers_draw_digit
	
	ret
; end

_pong_draw_paddle:
	; void (Player* player)
	
	push bp
	mov bp, sp
	
	push si
	
	; arg si(player)
	mov si, [bp+4]
	
	; dx = player.x
	mov dx, [si+0]
	; cx = player.y
	mov cx, [si+2]
	
	; video.draw_vertical_line(player.x, player.y, PADDLE_HEIGHT, PADDLE_COLOR)
	push PADDLE_COLOR
	push PADDLE_HEIGHT
	push cx
	push dx
	call video_draw_vertical_line
	add sp, 8
	
	pop si
	
	mov sp, bp
	pop bp
	ret
; end

_pong_draw_ball:
	push BALL_COLOR
	push word [BALL.Y]
	push word [BALL.X]
	call video_draw_point
	add sp, 6
	
	ret
; end

; Globals

PLAYER_1:
	.X : dw PADDLE_OFFSET
	.Y : dw (SCREEN_HEIGHT - PADDLE_HEIGHT)/2
	.VX: dw 0
	.VY: dw 0
	.SCORE: dw 0

PLAYER_2:
	.X : dw SCREEN_WIDTH - PADDLE_OFFSET
	.Y : dw (SCREEN_HEIGHT - PADDLE_HEIGHT)/2
	.VX: dw 0
	.VY: dw 0
	.SCORE: dw 0

SCORE_SUPPRESS: dw 0

BALL:
	.X : dw SCREEN_WIDTH/2
	.Y : dw SCREEN_HEIGHT/2
	.VX: dw 2
	.VY: dw 1

SOUND_BOUNCE_WALL:
	.LEN: dw 2
	.DAT: dw 12000, 4000

SOUND_BOUNCE_POINT:
	.LEN: dw 7
	.DAT: dw 1141, 1015, 898, 1015, 898, 854, 570

SOUND_BOUNCE_PADDLE:
	.LEN: dw 3
	.DAT: dw 4000, 12000
