numbers_draw_digit:
	; void (int digit[ax], int x[cx], int y[dx])
	
	push di
	push bx
	
	cmp ax, 11
	jl .notover
	mov ax, 10
	.notover:
	cmp ax, -1
	jg .notunder
	mov ax, 0
	.notunder:
	
	shl ax, 1
	mov di, ax
	mov ax, [DIGITS+di]
	
	; ax = digit bitmask, shifted right each step
	; cx = digit x base, const
	; dx = digit y base, const
	; bh = yctr, [0, 5)
	; bl = xctr, [0, 3)
	; di = tmp
	
	mov bh, 0
	.yloop:
		mov bl, 0
		.xloop:
			; if not ax & 1:
			; 	skip draw point
			mov di, ax
			and ax, 1
			mov ax, di
			jz .blank
			
			push ax
			push cx
			push dx
			
			; {
				; cx = x + xctr
				; dx = y + yctr
				
				; cx += bl
				mov di, bx
				mov bh, 0
				add cx, bx
				; dx += bh
				mov bx, di
				mov bl, bh
				mov bh, 0
				add dx, bx
				mov bx, di
				
				; draw point
				push 5
				push dx
				push cx
				call video_draw_point
				add sp, 6
			; }
			
			pop dx
			pop cx
			pop ax
			; end
			
			.blank:
			
			shr ax, 1
			
			add bl, 1
			cmp bl, 3
			jl .xloop
		add bh, 1
		cmp bh, 5
		jl .yloop
	
	.ret:
	pop bx
	pop di
	
	ret
; end

DIGITS:
	dw 0111101101101111b
	dw 0111010010011010b
	dw 0111001111100111b
	dw 0111100111100111b
	dw 0100100111101101b
	dw 0111100111001111b
	dw 0111101111001111b
	dw 0100100100100111b
	dw 0111101111101111b
	dw 0111100111101111b
	; question mark
	dw 0010000010100011b
