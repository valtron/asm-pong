sys_push_interrupt_handler:
	; void (i16 interrupt, f* handler, out f** old_handler)
	
	push bp
	mov bp, sp
	
	cli
	
	; sys.save_interrupt_handler(interrupt, out old_handler)
	mov cx, [bp+8]
	push cx
	mov ax, [bp+4]
	push ax
	call sys_save_interrupt_handler
	add sp, 4
	
	; sys.set_interrupt_handler(interrupt, handler)
	mov cx, [bp+6]
	push cx
	mov ax, [bp+4]
	push ax
	call sys_set_interrupt_handler
	add sp, 4
	
	sti
	
	mov sp, bp
	pop bp
	
	ret
; end

sys_save_interrupt_handler:
	; void (i16 interrupt, out f** old_handler)
	
	push bp
	mov bp, sp
	
	push bx
	push es
	push di
	
	; es:bx = current handler
	mov ah, 0x35
	; al = interrupt
	mov al, [bp+4]
	int 0x21
	
	; di = old handler offset
	mov di, [bp+6]
	
	; store offset
	mov [di], bx
	
	; di = old handler segment
	add di, 2
	
	; store segment
	mov [di], es
	
	pop di
	pop es
	pop bx
	
	mov sp, bp
	pop bp
	
	ret
; end

sys_set_interrupt_handler:
	; void (i16 interrupt, f* handler)
	push bp
	mov bp, sp
	
	push ds
	
	; ds:dx = handler
	mov dx, [bp+6]
	mov ax, cs
	mov ds, ax
	
	mov ah, 0x25
	; al = interrupt
	mov al, [bp+4]
	int 0x21
	
	pop ds
	
	mov sp, bp
	pop bp
	
	ret
; end

sys_pop_interrupt_handler:
	; void (i16 interrupt, f** old_handler)
	
	push bp
	mov bp, sp
	
	cli
	
	; sys.set_interrupt_handler(interrupt, old_handler)
	mov cx, [bp+6]
	push cx
	mov ax, [bp+4]
	push ax
	call sys_set_interrupt_handler
	add sp, 4
	
	sti
	
	mov sp, bp
	pop bp
	
	ret
; end

sys_set_mode_graphics:
	; Sys.SetMode(Mode.Graphics)
	mov ax, 0x13
	int 0x10
	
	ret
; end

sys_set_mode_text:
	; Sys.SetMode(Mode.Text)
	mov ax, 0x03
	int 0x10
	
	ret
; end

sys_exit:
	; DOS.Exit()
	mov ax, 0x00
	int 0x21
	ret
; end

sys_poll_keyboard:
	mov ax, 0x00
	int 0x16
	
	ret
; end
