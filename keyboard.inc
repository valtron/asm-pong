KEY_ESCAPE EQU 0x01
KEY_W      EQU 0x11
KEY_S      EQU 0x1F
KEY_UP     EQU 0x48
KEY_DOWN   EQU 0x50
KEY_MAX    EQU 0x80

keyboard_init:
	push OLD_0x09
	push _keyboard_on_event
	push 0x09
	call sys_push_interrupt_handler
	add sp, 6
	
	ret
; end

keyboard_destroy:
	push OLD_0x09
	push 0x09
	call sys_pop_interrupt_handler
	add sp, 4
	
	ret
; end

_keyboard_on_event:
	push ax
	push cx
	push di
	
	in al, 0x60
	
	; cl = 0 if key down, != 0 if key up
	mov cl, al
	and cl, 0x80
	xor cl, 0x80
	
	and al, 0x7F
	
	cmp al, KEY_MAX
	jae .ret
	
	; KEY_STATE[al] = cl
	mov di, KEY_STATE
	mov ah, 0
	add di, ax
	mov [di], cl
	
	.ret:
	
	pop di
	pop cx
	pop ax
	
	pushf
	call far dword [OLD_0x09]
	
	iret
; end

keyboard_wait_for_key:
	; void (i16[ax] key)
	
	push di
	
	mov di, ax
	
	.wait_for_esc:
		mov al, [di+KEY_STATE]
		cmp al, 0
		je .wait_for_esc
	
	pop di
	
	ret
; end

KEY_STATE:
	times KEY_MAX db 0

OLD_0x09:
	dw 0
	dw 0
