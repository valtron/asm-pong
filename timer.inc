timer_init:
	; void (f*[ax] handler)
	
	mov [NEW_0x1C], ax
	
	push OLD_0x1C
	push _timer_tick_wrapper
	push 0x1C
	call sys_push_interrupt_handler
	add sp, 6
	
	ret
; end

timer_destroy:
	push OLD_0x1C
	push 0x1C
	call sys_pop_interrupt_handler
	add sp, 4
	
	ret
; end

_timer_tick_wrapper:
	mov ax, [NEW_0x1C]
	call ax
	
	; call old int
	pushf
	call far dword [OLD_0x1C]
	
	iret
; end

NEW_0x1C:
	dw 0
	dw 0

OLD_0x1C:
	dw 0
	dw 0
