utils_clip_to_bounds:
	; i16 x'[ax], i16 was_clipped[cx] (i16 x[ax], i16 min[cx], i16 max[dx])
	
	cmp ax, cx
	jl .below
	
	cmp ax, dx
	jge .above
	
	mov cx, 0
	jmp .ret
	
	; if x >= max:
	.above:
		; ret max - ((x - max) + 1)
		add dx, dx
		sub dx, ax
		mov ax, dx
		sub ax, 1
		mov cx, 1
		jmp .ret
	
	; if x < min:
	.below:
		; ret min + (min - x)
		add cx, cx
		sub cx, ax
		mov ax, cx
		mov cx, 1
		jmp .ret
	
	.ret:
	ret
;
