VIDEO_MEM_BASE EQU 0xA000

SCREEN_WIDTH  EQU 320
SCREEN_HEIGHT EQU 200
SCREEN_SIZE_WORDS = SCREEN_HEIGHT * SCREEN_WIDTH / 2

; Segment of memory to use - crashes if < 0x1192
SCREEN_BUFFER EQU 0x1200

video_init:
	call sys_set_mode_graphics
	
	ret
; end

video_destroy:
	call sys_set_mode_text
	
	ret
; end

video_copy_buffer_to_screen:
	call _video_wait_for_retrace
	
	push ds
	push si
	push es
	push di
	
	mov cx, SCREEN_SIZE_WORDS
	
	push SCREEN_BUFFER
	pop ds
	mov si, 0
	
	push VIDEO_MEM_BASE
	pop es
	mov di, 0
	
	cld
	rep movsw
	
	pop di
	pop es
	pop si
	pop ds
	
	ret
; end

_video_wait_for_retrace:
	mov dx, 0x03DA
	
	.part1:
		in al, dx
		and al, 0x8
		jnz .part1
	
	.part2:
		in al, dx
		and al, 0x8
		jz .part2
	
	ret
; end

video_clear_screen:
	push es
	push di
	
	push SCREEN_BUFFER
	pop es
	mov di, 0
	
	mov ax, 0
	mov cx, SCREEN_SIZE_WORDS
	cld
	rep stosw
	
	pop di
	pop es
	ret
; end

video_draw_point:
	; void (i16 x, i16 y, i16 coloridx)
	
	; prologue
	push bp
	mov bp, sp
	
	; save registers
	push di
	push es
	
	; arg x
	mov cx, [bp+4]
	; arg y
	mov dx, [bp+6]
	; arg coloridx
	mov ax, [bp+8]
	
	; var di(offset) = y * SCREEN_WIDTH + x
	imul dx, dx, SCREEN_WIDTH
	add dx, cx
	mov di, dx
	
	; video.put(offset, coloridx)
	push SCREEN_BUFFER
	pop es
	mov word [es:di], ax
	
	; restore registers
	pop es
	pop di
	
	; epilogue
	mov sp, bp
	pop bp
	ret
; end

video_draw_vertical_line:
	; void (i16 x, i16 y, i16 len, i16 coloridx)
	
	; prologue
	push bp
	mov bp, sp
	
	; save registers
	push di
	push es
	push bx
	
	; arg x
	mov ax, [bp+4]
	; arg y
	mov bx, [bp+6]
	; arg len
	mov cx, [bp+8]
	
	push SCREEN_BUFFER
	pop es
	
	; cx(offset1) = (y + len) * SCREEN_WIDTH + x
	add cx, bx
	imul cx, cx, SCREEN_WIDTH
	add cx, ax
	
	; bx(offset0) = y * SCREEN_WIDTH + x
	imul bx, bx, SCREEN_WIDTH
	add bx, ax
	
	; arg coloridx
	mov ax, [bp+10]
	
	.loop_top:
		cmp bx, cx
		jae .loop_end
		
		mov di, bx
		mov word [es:di], ax
		
		add bx, SCREEN_WIDTH
		jmp .loop_top
	
	.loop_end:
	
	; restore registers
	pop bx
	pop es
	pop di
	
	; epilogue
	mov sp, bp
	pop bp
	ret
; end

video_draw_horizontal_line:
	; void (i16 x, i16 y, i16 len, i16 coloridx)
	
	push bp
	mov bp, sp
	
	; save registers
	push di
	push es
	push bx
	
	; arg x
	mov ax, [bp+4]
	; arg y
	mov bx, [bp+6]
	; arg len
	mov cx, [bp+8]
	
	push SCREEN_BUFFER
	pop es
	
	; bx(offset0) = y * SCREEN_WIDTH + x
	imul bx, bx, SCREEN_WIDTH
	add bx, ax
	
	; cx(offset1) = y * SCREEN_WIDTH + x + len
	add cx, bx
	
	; arg coloridx
	mov ax, [bp+10]
	
	.loop_top:
		cmp bx, cx
		jae .loop_end
		
		mov di, bx
		mov word [es:di], ax
		
		add bx, 1
		jmp .loop_top
	
	.loop_end:
	
	; restore registers
	pop bx
	pop es
	pop di
	
	mov sp, bp
	pop bp
	ret
; end
