format binary
org 0x0100

jmp main

include 'system.inc'
include 'video.inc'
include 'keyboard.inc'
include 'audio.inc'
include 'utils.inc'
include 'timer.inc'
include 'numbers.inc'
include 'pong.inc'

main:
	call main_init
	
	mov ax, KEY_ESCAPE
	call keyboard_wait_for_key
	
	call main_destroy
	
	call sys_exit
; end

main_init:
	call video_init
	call keyboard_init
	
	mov ax, pong_tick
	call timer_init
	
	ret
; end

main_destroy:
	call timer_destroy
	
	call keyboard_destroy
	call video_destroy
	
	ret
; end
