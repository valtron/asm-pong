audio_play_sound:
	; void (Sound*[ax] sound)
	
	push ax
	call _audio_speaker_on
	pop ax
	
	push di
	
	mov di, ax
	
	add ax, 2
	mov [SOUND_PLAYING.SOUND], ax
	mov ax, [di+0]
	mov [SOUND_PLAYING.CNT], ax
	
	pop di
	
	ret
; end

audio_play_tick:
	; void ()
	
	mov ax, [SOUND_PLAYING.SOUND]
	cmp ax, 0
	jz .ret
	
	mov cx, [SOUND_PLAYING.CNT]
	cmp cx, 0
	
	push di
	
	mov di, ax
	
	add ax, 2
	mov [SOUND_PLAYING.SOUND], ax
	
	mov ax, [di]
	call _audio_set_freq
	
	pop di
	
	dec cx
	mov [SOUND_PLAYING.CNT], cx
	
	jnz .ret
	
	.stop:
	mov [SOUND_PLAYING.SOUND], word 0
	call _audio_speaker_off
	
	.ret:
	ret
; end

_audio_set_freq:
	; void (i16[ax] invfreq)
	
	out 0x42, al
	mov al, ah
	out 0x42, al
	
	ret
; end

_audio_speaker_on:
	in al, 0x61
	or al, 00000011b
	out 0x61, al
	
	; connect to timer 2
	mov al, 10110110b
	out 0x43, al
	
	ret
; end

_audio_speaker_off:
	in al, 0x61
	and al, 11111100b
	out 0x61, al
	
	ret
; end

SOUND_PLAYING:
	.SOUND: dw 0
	.CNT: dw 0
